# If your Vagrant version is lower than 1.5, you can still use this provisioning
# by commenting or removing the line below and providing the config.vm.box_url parameter,
# if it's not already defined in this Vagrantfile. Keep in mind that you won't be able
# to use the Vagrant Cloud and other newer Vagrant features.
Vagrant.require_version ">= 1.5"

# Set default provider
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'

# Install vagrant lxc plugin
#required_plugins = %w( vagrant-lxc )
#required_plugins.each do |plugin|
#  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
#end


# Check to determine whether we're on a windows or linux/os-x host,
# later on we use this to launch ansible in the supported way
# source: https://stackoverflow.com/questions/2108727/which-in-ruby-checking-if-program-exists-in-path-from-ruby
def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
            exe = File.join(path, "#{cmd}#{ext}")
            return exe if File.executable? exe
        }
    end
    return nil
end

Vagrant.configure("2") do |config|
  config.vm.box = "debian-jessie64"
  config.vm.box_url = "http://store.internetdevels.com/files/data/processed/mula/jessie.box"
  #config.vm.box = "debian7.3.x64"
  #config.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/debian-73-x64-virtualbox-puppet.box"
  #config.vm.box = "ubuntu 14.04"
  #config.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

  # Enable host-only access to the machine using a specific IP.
  config.vm.network :private_network, ip: "192.168.33.3"
  config.vm.network :forwarded_port, host: 2200, guest: 22
  #config.vm.network :forwarded_port, host: 80, guest: 80
  #config.vm.network :forwarded_port, host: 443, guest: 443
  #config.vm.network :forwarded_port, host: 1080, guest: 1080
  #config.vm.network "private_network", ip: "192.168.2.100", lxc__bridge_name: 'vlxcbr1'

  config.vm.provider :virtualbox do |v|
    v.customize ["modifyvm", :id, "--name", "idevels"]
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--memory", 2048]
    v.customize ["modifyvm", :id, "--cpus", 2]
    v.customize ["modifyvm", :id, "--ioapic", "on"]
  end
 
  if (/darwin/ =~ RUBY_PLATFORM) != nil
    config.vm.synced_folder ".", "/var/www"
  elsif (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    config.vm.synced_folder ".", "/var/www"
  else
    config.vm.synced_folder ".", "/vagrant",:disabled => true
    config.vm.synced_folder ".", "/var/www", nfs: true
    config.nfs.map_uid = Process.uid
    config.nfs.map_gid = Process.gid
  end

# Ansible provisioning (you need to have ansible installed)    
  if which('ansible-playbook')
    config.vm.provision "ansible" do |ansible|
      ansible.playbook = "provisioning/playbook.yml"
      ansible.inventory_path = "provisioning/inventory"
      ansible.limit = 'all'
      # Run commands as root.
      ansible.sudo = true
      # ansible.raw_arguments = ['-v']
      # run only local enviroment
      ansible.tags = 'local'
    end
  else
    config.vm.provision :shell, path: "provisioning/provision.sh", args: ["default"]
  end

# Set the name of the VM. See: http://stackoverflow.com/a/17864388/100134
  config.vm.define :idevels do |idevels|
    idevels.vm.hostname = "idevels"
  end

# lxc config
#Vagrant.configure("2") do |config|
#  config.vm.define :lxcweb do |box|
#    box.vm.hostname = "lxcweb"
#    box.vm.box = "jessie"
#    box.vm.box_url = "http://store.internetdevels.com/files/data/processed/mula/lxc-jessie-amd64.box"
#    box.vm.network "private_network", ip: "192.168.5.100", lxc__bridge_name: 'vlxcbr1'
#    box.vm.synced_folder ".", "/var/www"
#    box.vm.provision "ansible" do |ansible|
#      ansible.playbook = "provisioning/playbook.yml"  
#      ansible.inventory_path = "provisioning/inventory"
#      ansible.limit = 'all'
#      ansible.sudo = true
#      # ansible.raw_arguments = ['-v']
#      ansible.tags = 'local_web'
#    end
#  end
#  config.vm.define :lxcdb do |box|
#    box.vm.hostname = "lxcdb"
#    box.vm.box = "jessie"
#    box.vm.box_url = "http://store.internetdevels.com/files/data/processed/mula/lxc-jessie-amd64.box"
#    box.vm.network "private_network", ip: "192.168.5.101", lxc__bridge_name: 'vlxcbr1'
#    box.vm.provision "ansible" do |ansible|
#      ansible.playbook = "provisioning/playbook.yml"  
#      ansible.inventory_path = "provisioning/inventory"
#      ansible.limit = 'all'
#      ansible.sudo = true
#      # ansible.raw_arguments = ['-v']
#      ansible.tags = 'local_db'
#    end
#  end

end
