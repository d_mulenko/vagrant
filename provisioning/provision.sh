#!/usr/bin/env bash
sudo apt-get update -qq

ANSIBLE_PATH=/usr/local/bin/ansible
LXC_PATH=/usr/bin/lxc-ls
VAGRANT_PATH=/usr/bin/vagrant
VAGRANT_LXC_PATH=/usr/local/bin/vagrant-lxc-wrapper
NFS_PATH=/usr/sbin/exportfs

if [[ ! -d $ANSIBLE_PATH ]]; then
	sudo DEBIAN_FRONTEND=noninteractive apt-get -yyqq install python-pip python-dev
	sudo pip install ansible -q
fi

#if [[ ! -d $LXC_PATH ]]; then
#	sudo DEBIAN_FRONTEND=noninteractive apt-get -yyqq install lxc redir bridge-utils
#fi

#if [[ ! -d $VAGRANT_PATH ]]; then	
#	sudo DEBIAN_FRONTEND=noninteractive apt-get -yyqq install vagrant
#fi

#if [[ ! -d $NFS_PATH ]]; then	
#	sudo DEBIAN_FRONTEND=noninteractive apt-get -yyqq nfs-server nfs-client
#fi

#if [[ ! -d $VAGRANT_LXC_PATH ]]; then
#    vagrant plugin install vagrant-lxc
#fi

sudo ansible-playbook /var/www/provisioning/playbook.yml --inventory-file=/var/www/provisioning/inventory --tags=local --connection=local