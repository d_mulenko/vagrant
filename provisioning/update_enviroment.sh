#!/usr/bin/env bash

ANSIBLE_PATH=/usr/local/bin/ansible

if [[ ! -d $ANSIBLE_PATH ]]; then
	sudo apt-get update -qq
	sudo DEBIAN_FRONTEND=noninteractive apt-get -yyqq install python-pip python-dev -qq
	sudo pip install ansible -q
fi

sudo ansible-playbook playbook.yml --inventory-file=inventory --tags=$1 --connection=local
